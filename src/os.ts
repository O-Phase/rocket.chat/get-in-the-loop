interface OS {
  readonly name: string;
  readonly icon: string;
}


export const oses: {[key: string]: OS} = {
	'windows': {'name': 'Windows', 'icon': ''},
	'linux':   {'name': 'Linux',   'icon': ''},
	'macos':   {'name': 'macOS',   'icon': ''},
	'android': {'name': 'Android', 'icon': ''},
	'ios':     {'name': 'iOS',     'icon': ''},
};

// export oses;