// See: https://cli.vuejs.org/config/

const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
	publicPath: './',
	productionSourceMap: true,
	css: {
		sourceMap: true,
	},
	transpileDependencies: true,
	devServer: {
		proxy: {
			'^/api': {
				target: 'https://chat.opha.se/',
				ws: true,
				changeOrigin: true,
			},
		},
	},
})
